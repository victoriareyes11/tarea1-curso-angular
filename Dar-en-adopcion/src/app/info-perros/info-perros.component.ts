import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { infoperros } from '../models/info-perros.model';
import {ListaPerrosComponent} from '../lista-perros/lista-perros.component';




@Component({
  
  selector: 'app-info-perros',
  templateUrl: './info-perros.component.html',
  styleUrls: ['./info-perros.component.css']
})
export class InfoPerrosComponent implements OnInit {
 @Input() perros: infoperros;

 
 @HostBinding('attr.class') cssClass="col-md-3";
  constructor(private listaPerrosComponent : ListaPerrosComponent) { }

  ngOnInit(): void {
  }
 

}



