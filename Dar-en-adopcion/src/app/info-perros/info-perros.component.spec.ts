import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoPerrosComponent } from './info-perros.component';

describe('InfoPerrosComponent', () => {
  let component: InfoPerrosComponent;
  let fixture: ComponentFixture<InfoPerrosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoPerrosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoPerrosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
