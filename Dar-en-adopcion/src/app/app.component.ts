import { Component } from '@angular/core';
import { InfoPerrosComponent } from './info-perros/info-perros.component';
import { ListaPerrosComponent} from './lista-perros/lista-perros.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Dar-en-adopcion';
}
