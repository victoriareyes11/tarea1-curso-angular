import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InfoPerrosComponent } from './info-perros/info-perros.component';
import { ListaPerrosComponent } from './lista-perros/lista-perros.component';

@NgModule({
  declarations: [
    AppComponent,
    InfoPerrosComponent,
    ListaPerrosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
