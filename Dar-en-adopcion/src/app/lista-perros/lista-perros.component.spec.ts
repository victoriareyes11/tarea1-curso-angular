import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaPerrosComponent } from './lista-perros.component';

describe('ListaPerrosComponent', () => {
  let component: ListaPerrosComponent;
  let fixture: ComponentFixture<ListaPerrosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaPerrosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaPerrosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
