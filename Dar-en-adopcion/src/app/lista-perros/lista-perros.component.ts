import { Component, OnInit, Input } from '@angular/core';
import {infoperros} from './../models/info-perros.model';


@Component({
  selector: 'app-lista-perros',
  templateUrl: './lista-perros.component.html',
  styleUrls: ['./lista-perros.component.css']
})
export class ListaPerrosComponent implements OnInit {
  perros: infoperros[];
  url=" ";
  


  constructor() { 
    this.perros=[];
  }

  ngOnInit(): void {
  }

  
  MyMethod(deviceValue):string{
    console.log(deviceValue);
    if(deviceValue=="Conejo"){
        this.url="assets/images/Conejo.png";
        console.log(this.url);
      }
    
    if(deviceValue=="Perro"){
        this.url="assets/images/Perro.png";
        console.log(this.url);
      }
    
    if(deviceValue=="Gato"){
        this.url="assets/images/Gato.png";
        console.log(this.url);
      }
   
    return this.url;
  }

  guardar(nombre:string, tamano:string, raza:string, edad:Int16Array, sexo:string):boolean {
    this.perros.push(new infoperros(nombre,tamano,raza, edad, sexo, this.url));
    
    return false;

  }
  
}


